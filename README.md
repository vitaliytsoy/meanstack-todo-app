# MEAN Stack TODO Project

The project implements simple todo task manager with MEAN Stack Techmologies used.
MEAN - Mongodb Express Angular Nodejs

## Prerequisites

Things you need to install

```
Nodejs
npm
webpack
mongodb
nodemon
```

## Set Up


```
# Clone repo

git clone https://vitaliytsoy@bitbucket.org/vitaliytsoy/meanstack-todo-app.git
cd meanstack-todo-app


# install node dependencies
npm install

# create webpack build
webpack

# start mongo in separate terminal
mongod

# view the app
node src/app.js
or
nodemon

# Note: to develop, see instructions below
```


## Usage
```
# nodemon will watch for file changes in the app
# and restart the server
nodemon

# Go to localhost:3000

```

## Project Structure

Project's Folders:
'app' contains angular app files
'mock' it contains data for mongodb, but it useles because it was rebuild another way.
'public' contains templates and as it says public version of app
'src' contains Express and MongoDB related files


## Developing
```
# it is up to you what tools to use
# i personaly prefer these

# some helpfull additional tools
npm install -g nodemon


# nodemon will watch for file changes in the app
# and restart the server
nodemon --debug app.js
```

## Need to be repaired
After completion of the task (checkbox), if you will try to save and reload page. It will appear that it do not save completed tasks.