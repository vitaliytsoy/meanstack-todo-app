'use strict';

var Todo = require('./models/todo.js');

var todos = [
	'Task 1 AHAHA',
	'Task 2 EHEHE',
	'Task 3 UHUHU'
];

todos.forEach(function(todo, index){
	Todo.find({'name': todo}, function(err, todos){
		if(!err && !todos.length){
			Todo.create({completed: false, name: todo});
		};
	});
});